########## Importer les modules necessaires ##############
from tkinter import *
from random import *
import time
from tkinter.font import Font

phrase = None

def Plateau(): 
    '''
    fonction qui crée un plateau en suivant l'ordre de la liste
    numérotation des cases 
    lance la fonction joueur
    '''
    a=case[0][0]
    b=case[0][1]#début numérotation des cases#
    num_case=0
    Canevas.create_rectangle(a-40,b-40,a+40,b+40,fill='#CEECF5')
    a=case[1][0]
    b=case[1][1]
    for i in range(1,len(case)):            #creation du plateau en fonction de la liste
            Canevas.create_rectangle(a-40,b-40,a+40,b+40,fill='#CEECF5')
            Canevas.create_text(a+30,b+30,text=str(num_case))
            num_case+=1
            a=case[0+i][0]
            b=case[0+i][1] 
    Canevas.create_text(case[0],text="DEPART",fill="black")
    Canevas.create_text(case[3],text="+21",fill="black")  
    Canevas.create_text(case[5],text="+3",fill="black")
    Canevas.create_text(case[10],text="-2",fill="black")
    Canevas.create_text(case[12],text="BONUS",fill="black")
    Canevas.create_text(case[18],text="-10",fill="black")
    Canevas.create_text(case[20],text="BONUS",fill="black")
    Canevas.create_text(case[22],text="-5",fill="black")
    Canevas.create_text(case[25],text="MALUS",fill="black")
    Canevas.create_text(case[32],text="+3",fill="black")
    Canevas.create_text(case[34],text="MALUS",fill="black")
    Canevas.create_text(case[36],text="MORT",fill="black")
    Canevas.create_text(case[39],text="ARRIVEE",fill="black")
    bouton_plateau=Button(Mafenetre, text="PLATEAU", command=Plateau)
    bouton_plateau.place(x=100,y=600)
    bouton_regles=Button(Mafenetre, text="REGLES", command=Regles)
    bouton_regles.place(x=300,y=600)
    bouton_lancer=Button(Mafenetre, text="LANCER", command=Dé)
    bouton_lancer.place(x=200,y=500)
    Joueur()
    
    
def Regles(): # affichage des regles en dessous du plateau #
    Canevas.create_rectangle(50,700,900,850,fill='black')
    Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
    Canevas.create_text(450,725,text="n°1: Quand le joueur tombe sur +3, il avance de 3 cases",fill="white",font=Mapolice)
    Canevas.create_text(450,750,text="n°2: Quand le joueur tombe sur -3, il recule de 3 cases",fill="white",font=Mapolice)
    Canevas.create_text(450,775,text="n°3: Quand le joueur tombe sur BONUS, il avance aléatoirement de 1 à 4 cases",fill="white",font=Mapolice)
    Canevas.create_text(450,800,text="n°4: Quand le joueur tombe sur MALUS, il recule aléatoirement de 1 à 4 cases",fill="white",font=Mapolice)
    Canevas.create_text(450,825,text="n°5: Quand le joueur tombe sur Mort, il recule directement à la case départ",fill="white",font=Mapolice)
    
def Dé(): # affichage du dé #
    '''
    fonction qui détermine un nombre aleatoire et qui affiche un dé représentant le nombre obtenu
    quand on clique sur le bouton lancer, nbr_lancer prend +1
    
    '''
    global nbr_lancer
    Canevas.create_rectangle(800,200,900,300,fill='white')
    nombre=randint(1,6)
    print(nbr_lancer)
    if nombre==1:
        Canevas.create_rectangle(845,245,855,255,fill='black')
    elif nombre==2:
        Canevas.create_rectangle(870,220,880,230,fill='black')
        Canevas.create_rectangle(820,270,830,280,fill='black')
    elif nombre==3:
        Canevas.create_rectangle(870,220,880,230,fill='black')
        Canevas.create_rectangle(820,270,830,280,fill='black')
        Canevas.create_rectangle(845,245,855,255,fill='black')
    elif nombre==4:
        Canevas.create_rectangle(870,220,880,230,fill='black')
        Canevas.create_rectangle(820,270,830,280,fill='black')
        Canevas.create_rectangle(870,270,880,280,fill='black')
        Canevas.create_rectangle(820,220,830,230,fill='black')
    elif nombre==5:
        Canevas.create_rectangle(870,220,880,230,fill='black')
        Canevas.create_rectangle(820,270,830,280,fill='black')
        Canevas.create_rectangle(870,270,880,280,fill='black')
        Canevas.create_rectangle(820,220,830,230,fill='black')
        Canevas.create_rectangle(845,245,855,255,fill='black')
    elif nombre==6:
        Canevas.create_rectangle(870,220,880,230,fill='black')
        Canevas.create_rectangle(820,270,830,280,fill='black')
        Canevas.create_rectangle(870,270,880,280,fill='black')
        Canevas.create_rectangle(820,220,830,230,fill='black')
        Canevas.create_rectangle(820,245,830,255,fill='black')
        Canevas.create_rectangle(870,245,880,255,fill='black')
    nbr_lancer+=1
    Pion(nombre)
    
def Pion(dé): 
    '''
    deplace le pion: change les coordonnées
    change la position des joueurs
    '''
    global pos
    global Joueur
    for i in range(100):
        if nbr_lancer==1+(4*i):
            joueur_num1=Canevas.create_oval(case[dé+pos[0]][0]-30,case[dé+pos[0]][1]-30,case[dé+pos[0]][0]-10,case[dé+pos[0]][1]-10,fill='red')
            pos[0]+=dé
            print(pos)
            action(case)
        if nbr_lancer==2+(4*i):
            joueur_num4=Canevas.create_oval(case[dé+pos[1]][0]+30,case[dé+pos[1]][1]-30,case[dé+pos[1]][0]+10,case[dé+pos[1]][1]-10,fill='black')
            pos[1]+=dé
            print(pos)
            action(case)
        if nbr_lancer==3+(4*i):
            joueur_num3=Canevas.create_oval(case[dé+pos[2]][0]-30,case[dé+pos[2]][1]+30,case[dé+pos[2]][0]-10,case[dé+pos[2]][1]+10,fill='green')
            pos[2]+=dé
            print(pos)
            action(case)
        if nbr_lancer==4+(4*i):
            joueur_num2=Canevas.create_oval(case[dé+pos[3]][0]+30,case[dé+pos[3]][1]+30,case[dé+pos[3]][0]+10,case[dé+pos[3]][1]+10,fill='yellow')
            pos[3]+=dé
            print(pos)
            action(case)
    arrivee()
        
            

    

def action(case):
    '''
    si case=3: le pion avance en case 24  #y
    si case=5: le pion avance en case 8   #y
    si case=10: le pion recule en case 8  #
    si case=12: le pion avance aleatoirement de 1 à 4 cases #
    si case=15: le pion avanve de 0 pendant 1 tour
    si case=20: le pion avance aleatoirement de 1 à 4 cases #
    si case=22: le pion recule en case 17  #
    si case=25: le pion recule aleatoirement de 1 à 4 cases #
    si case=29: le pion avance de 0 pendant 1 tour
    si case=32: le pion avance en case 35  #
    si case=34: le pion recule aleatoirement de 1 à 4 cases #
    si case=36: le pion recule en case 1  #
    '''
    
    global phrase
    
    if phrase:
        Canevas.delete(phrase)
    
    
    if pos[0]==3:               #case 3== +21
        pos[0]=24
        joueur_num1=Canevas.create_oval(case[pos[0]][0]-30,case[pos[0]][1]-30,case[pos[0]][0]-10,case[pos[0]][1]-10,fill='red')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 1 avance de 21 cases",fill="black",font=Mapolice)
    if pos[1]==3:
        pos[1]=24
        joueur_num2=Canevas.create_oval(case[pos[1]][0]+30,case[pos[1]][1]-30,case[pos[1]][0]+10,case[pos[1]][1]-10,fill='black')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 2 avance de 21 cases",fill="black",font=Mapolice)
    if pos[2]==3:
        pos[2]=24
        joueur_num3=Canevas.create_oval(case[pos[2]][0]-30,case[pos[2]][1]+30,case[pos[2]][0]-10,case[pos[2]][1]+10,fill='green')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 3 avance de 21 cases",fill="black",font=Mapolice)
    if pos[3]==3:
        pos[3]=24
        joueur_num4=Canevas.create_oval(case[pos[3]][0]+30,case[pos[3]][1]+30,case[pos[3]][0]+10,case[pos[3]][1]+10,fill='yellow')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 4 avance de 21 cases",fill="black",font=Mapolice)
    if pos[0]==5:                   #case 5== +3
        pos[0]=8
        joueur_num1=Canevas.create_oval(case[pos[0]][0]-30,case[pos[0]][1]-30,case[pos[0]][0]-10,case[pos[0]][1]-10,fill='red')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 1 avance de 3 cases",fill="black",font=Mapolice)
    if pos[1]==5:
        pos[1]=8
        joueur_num2=Canevas.create_oval(case[pos[1]][0]+30,case[pos[1]][1]-30,case[pos[1]][0]+10,case[pos[1]][1]-10,fill='black')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 2 avance de 3 cases",fill="black",font=Mapolice)
    if pos[2]==5:
        pos[2]=8
        joueur_num3=Canevas.create_oval(case[pos[2]][0]-30,case[pos[2]][1]+30,case[pos[2]][0]-10,case[pos[2]][1]+10,fill='green')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 3 avance de 3 cases",fill="black",font=Mapolice)
    if pos[3]==5:
        pos[3]=8
        joueur_num4=Canevas.create_oval(case[pos[3]][0]+30,case[pos[3]][1]+30,case[pos[3]][0]+10,case[pos[3]][1]+10,fill='yellow')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 4 avance de 3 cases",fill="black",font=Mapolice)
    if pos[0]==10:                  #case 10== -2
        pos[0]=8
        joueur_num1=Canevas.create_oval(case[pos[0]][0]-30,case[pos[0]][1]-30,case[pos[0]][0]-10,case[pos[0]][1]-10,fill='red')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 1 recule de 2 cases",fill="black",font=Mapolice)
    if pos[1]==10:
        pos[1]=8
        joueur_num2=Canevas.create_oval(case[pos[1]][0]+30,case[pos[1]][1]-30,case[pos[1]][0]+10,case[pos[1]][1]-10,fill='black')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 2 recule de 2 cases",fill="black",font=Mapolice)
    if pos[2]==10:
        pos[2]=8
        joueur_num3=Canevas.create_oval(case[pos[2]][0]-30,case[pos[2]][1]+30,case[pos[2]][0]-10,case[pos[2]][1]+10,fill='green')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 3 recule de 2 cases",fill="black",font=Mapolice)
    if pos[3]==10:
        pos[3]=8
        joueur_num4=Canevas.create_oval(case[pos[3]][0]+30,case[pos[3]][1]+30,case[pos[3]][0]+10,case[pos[3]][1]+10,fill='yellow')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 4 recule de 2 cases",fill="black",font=Mapolice)
    if pos[0]==12:              #case12== aléatoire +1à4
        pos[0]=12+randint(1,4)
        joueur_num1=Canevas.create_oval(case[pos[0]][0]-30,case[pos[0]][1]-30,case[pos[0]][0]-10,case[pos[0]][1]-10,fill='red')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 1 avance aléatoirement de 1 à 4 cases",fill="black",font=Mapolice)
    if pos[1]==12:
        pos[1]=12+randint(1,4)
        joueur_num2=Canevas.create_oval(case[pos[1]][0]+30,case[pos[1]][1]-30,case[pos[1]][0]+10,case[pos[1]][1]-10,fill='black')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 2 avance aléatoirement de 1 à 4 cases",fill="black",font=Mapolice)
    if pos[2]==12:
        pos[2]=12+randint(1,4)
        joueur_num3=Canevas.create_oval(case[pos[2]][0]-30,case[pos[2]][1]+30,case[pos[2]][0]-10,case[pos[2]][1]+10,fill='green')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 3 avance aléatoirement de 1 à 4 cases",fill="black",font=Mapolice)
    if pos[3]==12:
        pos[3]=12+randint(1,4)
        joueur_num4=Canevas.create_oval(case[pos[3]][0]+30,case[pos[3]][1]+30,case[pos[3]][0]+10,case[pos[3]][1]+10,fill='yellow')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 4 avance aléatoirement de 1 à 4 cases",fill="black",font=Mapolice)
    if pos[0]==20:                  #case20== aléatoire +1à4
        pos[0]=20+randint(1,4)
        joueur_num1=Canevas.create_oval(case[pos[0]][0]-30,case[pos[0]][1]-30,case[pos[0]][0]-10,case[pos[0]][1]-10,fill='red')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 1 avance aléatoirement de 1 à 4 cases",fill="black",font=Mapolice)
    if pos[1]==20:
        pos[1]=20+randint(1,4)
        joueur_num2=Canevas.create_oval(case[pos[1]][0]+30,case[pos[1]][1]-30,case[pos[1]][0]+10,case[pos[1]][1]-10,fill='black')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 2 avance aléatoirement de 1 à 4 cases",fill="black",font=Mapolice)
        pos[2]=20+randint(1,4)
        joueur_num3=Canevas.create_oval(case[pos[2]][0]-30,case[pos[2]][1]+30,case[pos[2]][0]-10,case[pos[2]][1]+10,fill='green')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 3 avance aléatoirement de 1 à 4 cases",fill="black",font=Mapolice)
    if pos[3]==20:
        pos[3]=20+randint(1,4)
        joueur_num4=Canevas.create_oval(case[pos[3]][0]+30,case[pos[3]][1]+30,case[pos[3]][0]+10,case[pos[3]][1]+10,fill='yellow')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 4 avance aléatoirement de 1 à 4 cases",fill="black",font=Mapolice)
    if pos[0]==25:                  #case25== aléatoire -1à4
        pos[0]=25-randint(1,4)
        joueur_num1=Canevas.create_oval(case[pos[0]][0]-30,case[pos[0]][1]-30,case[pos[0]][0]-10,case[pos[0]][1]-10,fill='red')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 1 recule aléatoirement de 1 à 4 cases",fill="black",font=Mapolice)
    if pos[1]==25:
        pos[1]=25-randint(1,4)
        joueur_num2=Canevas.create_oval(case[pos[1]][0]+30,case[pos[1]][1]-30,case[pos[1]][0]+10,case[pos[1]][1]-10,fill='black')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 2 recule aléatoirement de 1 à 4 cases",fill="black",font=Mapolice)
    if pos[2]==25:
        pos[2]=25-randint(1,4)
        joueur_num3=Canevas.create_oval(case[pos[2]][0]-30,case[pos[2]][1]+30,case[pos[2]][0]-10,case[pos[2]][1]+10,fill='green')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 3 recule aléatoirement de 1 à 4 cases",fill="black",font=Mapolice)
    if pos[3]==25:
        pos[3]=25-randint(1,4)
        joueur_num4=Canevas.create_oval(case[pos[3]][0]+30,case[pos[3]][1]+30,case[pos[3]][0]+10,case[pos[3]][1]+10,fill='yellow')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 4 recule aléatoirement de 1 à 4 cases",fill="black",font=Mapolice)
    if pos[0]==34:                  #case34 == aléatoire -1à4
        pos[0]=34-randint(1,4)
        joueur_num1=Canevas.create_oval(case[pos[0]][0]-30,case[pos[0]][1]-30,case[pos[0]][0]-10,case[pos[0]][1]-10,fill='red')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 1 recule aléatoirement de 1 à 4 cases",fill="black",font=Mapolice)
    if pos[1]==34:
        pos[1]=34-randint(1,4)
        joueur_num2=Canevas.create_oval(case[pos[1]][0]+30,case[pos[1]][1]-30,case[pos[1]][0]+10,case[pos[1]][1]-10,fill='black')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 2 recule aléatoirement de 1 à 4 cases",fill="black",font=Mapolice)
    if pos[2]==34:
        pos[2]=34-randint(1,4)
        joueur_num3=Canevas.create_oval(case[pos[2]][0]-30,case[pos[2]][1]+30,case[pos[2]][0]-10,case[pos[2]][1]+10,fill='green')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 3 recule aléatoirement de 1 à 4 cases",fill="black",font=Mapolice)
    if pos[3]==34:
        pos[3]=34-randint(1,4)
        joueur_num4=Canevas.create_oval(case[pos[3]][0]+30,case[pos[3]][1]+30,case[pos[3]][0]+10,case[pos[3]][1]+10,fill='yellow')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 4 recule aléatoirement de 1 à 4 cases",fill="black",font=Mapolice)
    if pos[0]==22:
        pos[0]=17
        joueur_num1=Canevas.create_oval(case[pos[0]][0]-30,case[pos[0]][1]-30,case[pos[0]][0]-10,case[pos[0]][1]-10,fill='red')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 1 recule de 5 cases",fill="black",font=Mapolice)
    if pos[1]==22:
        pos[1]=17
        joueur_num2=Canevas.create_oval(case[pos[1]][0]+30,case[pos[1]][1]-30,case[pos[1]][0]+10,case[pos[1]][1]-10,fill='black')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 2 recule de 5 cases",fill="black",font=Mapolice)
    if pos[2]==22:
        pos[2]=17
        joueur_num3=Canevas.create_oval(case[pos[2]][0]-30,case[pos[2]][1]+30,case[pos[2]][0]-10,case[pos[2]][1]+10,fill='green')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 3 recule de 5 cases",fill="black",font=Mapolice)
    if pos[3]==22:
        pos[3]=17
        joueur_num4=Canevas.create_oval(case[pos[3]][0]+30,case[pos[3]][1]+30,case[pos[3]][0]+10,case[pos[3]][1]+10,fill='yellow')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 4 avance de 5 cases",fill="black",font=Mapolice)
    if pos[0]==32:
        pos[0]=35
        joueur_num1=Canevas.create_oval(case[pos[0]][0]-30,case[pos[0]][1]-30,case[pos[0]][0]-10,case[pos[0]][1]-10,fill='red')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 1 avance de 3 cases",fill="black",font=Mapolice)
    if pos[1]==32:
        pos[1]=35
        joueur_num2=Canevas.create_oval(case[pos[1]][0]+30,case[pos[1]][1]-30,case[pos[1]][0]+10,case[pos[1]][1]-10,fill='black')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 2 avance de 3 cases",fill="black",font=Mapolice)
    if pos[2]==32:
        pos[2]=35
        joueur_num3=Canevas.create_oval(case[pos[2]][0]-30,case[pos[2]][1]+30,case[pos[2]][0]-10,case[pos[2]][1]+10,fill='green')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 3 avance de 3 cases",fill="black",font=Mapolice)
    if pos[3]==32:
        pos[3]=35
        joueur_num4=Canevas.create_oval(case[pos[3]][0]+30,case[pos[3]][1]+30,case[pos[3]][0]+10,case[pos[3]][1]+10,fill='yellow')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 4 avance de 3 cases",fill="black",font=Mapolice)
    if pos[0]==36:
        pos[0]=1
        joueur_num1=Canevas.create_oval(case[pos[0]][0]-30,case[pos[0]][1]-30,case[pos[0]][0]-10,case[pos[0]][1]-10,fill='red')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 1 retourne en case 1",fill="black",font=Mapolice)
    if pos[1]==36:
        pos[1]=1
        joueur_num2=Canevas.create_oval(case[pos[1]][0]+30,case[pos[1]][1]-30,case[pos[1]][0]+10,case[pos[1]][1]-10,fill='black')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 2 retourne en case 1",fill="black",font=Mapolice)
    if pos[2]==36:
        pos[2]=1
        joueur_num3=Canevas.create_oval(case[pos[2]][0]-30,case[pos[2]][1]+30,case[pos[2]][0]-10,case[pos[2]][1]+10,fill='green')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 3 retourne en case 1",fill="black",font=Mapolice)
    if pos[3]==36:
        pos[3]=1
        joueur_num4=Canevas.create_oval(case[pos[3]][0]+30,case[pos[3]][1]+30,case[pos[3]][0]+10,case[pos[3]][1]+10,fill='yellow')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 4 retourne en case 1",fill="black",font=Mapolice)
    if  pos[0]==18:                  #case 18=-10
        pos[0]=8
        joueur_num1=Canevas.create_oval(case[pos[0]][0]-30,case[pos[0]][1]-30,case[pos[0]][0]-10,case[pos[0]][1]-10,fill='red')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 1 recule de 10 cases",fill="black",font=Mapolice)
    if pos[1]==18:
        pos[1]=8
        joueur_num2=Canevas.create_oval(case[pos[1]][0]+30,case[pos[1]][1]-30,case[pos[1]][0]+10,case[pos[1]][1]-10,fill='black')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 2 recule de 10 cases",fill="black",font=Mapolice)
    if pos[2]==18:
        pos[2]=8
        joueur_num3=Canevas.create_oval(case[pos[2]][0]-30,case[pos[2]][1]+30,case[pos[2]][0]-10,case[pos[2]][1]+10,fill='green')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 3 recule de 10 cases",fill="black",font=Mapolice)
    if pos[3]==18:
        pos[3]=8
        joueur_num4=Canevas.create_oval(case[pos[3]][0]+30,case[pos[3]][1]+30,case[pos[3]][0]+10,case[pos[3]][1]+10,fill='yellow')
        Mapolice = Font(family='Liberation Serif', size=10) # création d'une police pour l'affichage du texte
        phrase = Canevas.create_text(865,400,text="Le joueur 4 recule de 10 cases",fill="black",font=Mapolice)
    
    






def Joueur():
    '''
    affiche la position des 4 joueurs au départ
    '''
    joueur_num1=Canevas.create_oval(case[0][0]-30,case[0][1]-30,case[0][0]-10,case[0][1]-10,fill='red')
    joueur_num2=Canevas.create_oval(case[0][0]+30,case[0][1]-30,case[0][0]+10,case[0][1]-10,fill='black')
    joueur_num3=Canevas.create_oval(case[0][0]-30,case[0][1]+30,case[0][0]-10,case[0][1]+10,fill='green')
    joueur_num4=Canevas.create_oval(case[0][0]+30,case[0][1]+30,case[0][0]+10,case[0][1]+10,fill='yellow')
    
    
    
    


def arrivee():
    if pos[0] and pos[1] and pos[2]==39:
        Canevas.create_rectangle(0,0,1000,1000,fill='purple')
        Mapolice = Font(family='Liberation Serif', size=30) # création d'une police pour l'affichage du texte
        Canevas.create_text(500,400,text="Le jeu est fini, défaite du joueur numéro 4",fill="white",font=Mapolice)
    elif pos[0] and pos[1] and pos[3]==39:
        Canevas.create_rectangle(0,0,1000,1000,fill='purple')
        Mapolice = Font(family='Liberation Serif', size=30) # création d'une police pour l'affichage du texte
        Canevas.create_text(500,400,text="Le jeu est fini, défaite du joueur numéro 3",fill="white",font=Mapolice)
    elif pos[0] and pos[2] and pos[3]==39:
        Canevas.create_rectangle(0,0,1000,1000,fill='purple')
        Mapolice = Font(family='Liberation Serif', size=30) # création d'une police pour l'affichage du texte
        Canevas.create_text(500,400,text="Le jeu est fini, défaite du joueur numéro 2",fill="white",font=Mapolice)
    elif pos[1] and pos[2] and pos[3]==39:
         Canevas.create_rectangle(0,0,1000,1000,fill='purple')
         Mapolice = Font(family='Liberation Serif', size=30) # création d'une police pour l'affichage du texte
         Canevas.create_text(500,400,text="Le jeu est fini, défaite du joueur numéro 1",fill="white",font=Mapolice)
    
    
        
    

case=[[120,90],[200,90],[280,90],[360,90],[440,90],[520,90],[600,90],[680,90],[680,170],[680,250],[680,330],[680,410],[600,410],[520,410],[440,410],[360,410],[280,410],[200,410],[120,410],[120,330],[120,250],[120,170],[200,170],[280,170],[360,170],[440,170],[520,170],[600,170],[600,250],[600,330],[520,330],[440,330],[360,330],[280,330],[200,330],[200,250],[280,250],[360,250],[440,250],[520,250]]
'''
liste qui montre le milieu de chaque case dans l'ordre du plateau
'''
pos=[0,0,0,0]  #  position des joueurs 
nbr_lancer=0
NumJoueur=0   #0,1,2,3


Mafenetre = Tk()
Mafenetre.title("Titre")
Canevas = Canvas(Mafenetre,width=1000,height=1000,bg ='white')
Canevas.pack()
Mapolice = Font(family='Liberation Serif', size=15) # création d'une police pour l'affichage du texte


bouton_plateau=Button(Mafenetre, text="PLATEAU", command=Plateau)
bouton_plateau.place(x=100,y=600)
bouton_regles=Button(Mafenetre, text="REGLES", command=Regles)
bouton_regles.place(x=300,y=600)
bouton_lancer=Button(Mafenetre, text="LANCER", command=Dé)
bouton_lancer.place(x=200,y=500)

Mafenetre.mainloop()

